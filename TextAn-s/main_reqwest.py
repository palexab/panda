import json
import re
import requests

from bs4 import BeautifulSoup


session = requests.Session()
response = session.get(
    url="https://play.google.com/store/apps/details",
    params={
        "id": "com.tencent.ig",
        "hl": "ru",
        "showAllReviews": "true",
    },
)
soup = BeautifulSoup(response.text, "html.parser")
script = soup.find("script", attrs={"data-id": "_gd"})
fsid = json.loads(re.search("window.WIZ_global_data\s+=\s+(.+);$", script.text).group(1))["FdrFJe"]
data = re.search("ds:16(.*)</script>", response.text).group(1)

response = session.post(
    url="https://play.google.com/_/PlayStoreUi/data/batchexecute",
    params={
        "rpcids": "rYsCDe",
        "f.sid": fsid,
        "bl": "boq_playuiserver_20200113.05_p0",
        "hl": "ru",
        # "authuser": "",
        "soc-app": 121,
        "soc-platform": 1,
        "soc-device": 1,
        "_reqid": 84191,
        "rt": "c",
    },
    data={
        "f.req": r'[[["rYsCDe","[[\"com.tencent.ig\",7]]",null,"33"]]]',
    },
    headers={
        "X-Same-Domain": "1",
        "Referer": "https://play.google.com/",
    }
)
print(response.status_code)
print(response.text)
