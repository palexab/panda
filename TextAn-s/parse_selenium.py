from time import sleep

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

#executable_path="./geckodriver"
driver = webdriver.Chrome()
driver.get("https://play.google.com/store/apps/details?id=com.xdg.and.eu.lifeafter&hl=ru&showAllReviews=true")
sleep(5)

NEED = 1000
DEFAULT_ATTEMPTS = 3
DELAY = 2
last_count = -1
comments = []
attempts = DEFAULT_ATTEMPTS

while attempts > 0 and len(comments) < NEED:
    if last_count < len(comments):
        last_count = len(comments)
        attempts = DEFAULT_ATTEMPTS
    else:
        attempts -= 1
    try:
        button = driver.find_element_by_xpath("//span[@class='RveJvd snByac']")
        button.click()
    except NoSuchElementException:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    sleep(DELAY)
    comments = driver.find_elements_by_xpath("//div[@jscontroller='H6eOGe' and @jsmodel='y8Aajc']")
    print("%.2f%%" % (len(comments) / NEED * 100))

f = open('commit2.txt', 'w', encoding='utf-8')

new_comments = []
for comment in comments:
    mark = len(comment.find_elements_by_xpath(".//div[@class='vQHuPe bUWb7c']"))
    text = comment.find_element_by_xpath(".//span[@jsname='fbQN7e']").get_attribute("innerHTML")
    if not text:
        text = comment.find_element_by_xpath(".//span[@jsname='bN97Pc']").get_attribute("innerHTML")

    f.write(str(mark))
    f.write(" ")
    f.write(str(text))
    f.write('\t')
    new_comments.append({
        "mark": mark,
        "text": text,
    })
driver.close()
f.close()
